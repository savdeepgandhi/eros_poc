package com.theoplayer.poc_offlineplayback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.theoplayer.android.api.THEOplayerGlobal;

/**
 * Demo App Created by Savdeep Singh Gandhi on 17th April 2019
 * Copyright © 2019 Theoplayer.com. All rights reserved.
 */

// Main page of the test application.
// This is only used to show the different scenarios
public class MainActivity extends AppCompatActivity {

    Button onlinebutton;
    Button offlinebutton;
    Button taskoverviewbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView.setWebContentsDebuggingEnabled(true);
        // Init Theoplayer view
        setContentView(R.layout.activity_main);

        // Initialize cache for later usage (e.g. in page OfflinePlaybackWithSubtitles)
        // This is only required if the caching API will be used and can also be done using an event listener
        THEOplayerGlobal.getSharedInstance(this).setApplicationInstance(getApplication());

        // Online playback button logic
        onlinebutton = findViewById(R.id.online);
        onlinebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, OnlinePlayback.class);
                startActivity(i);
            }
        });

        // Task overview button logic
        taskoverviewbutton = findViewById(R.id.taskoverview);
        taskoverviewbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, TaskOverview.class);
                startActivity(i);
            }
        });

        // Offline playback button logic
        offlinebutton = findViewById(R.id.offline);
        offlinebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, OfflinePlaybackWithSubtitles.class);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
