package com.theoplayer.poc_offlineplayback;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.theoplayer.android.api.THEOplayerGlobal;
import com.theoplayer.android.api.cache.CachingTask;

import java.util.Iterator;

public class TaskOverview extends AppCompatActivity {

    Button backbutton;
    THEOplayerGlobal theoPlayerGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView.setWebContentsDebuggingEnabled(true);
        // Init Theoplayer view
        setContentView(R.layout.activity_taskoverview);

        // Get our global cache instance
        theoPlayerGlobal = THEOplayerGlobal.getSharedInstance(this);

        // Loop through the cached tasks
        Iterator cachedVideos = theoPlayerGlobal.getCache().getTasks().iterator();
        while(cachedVideos.hasNext()) {
            final CachingTask cachedVideo = (CachingTask) cachedVideos.next();

            // Create a new button with some of the cache info
            Button myButton = new Button(this);
            myButton.setText(
                    cachedVideo.getSource().getSources().get(0).getSrc() +
                    "\nAsset size:" + Long.toString(cachedVideo.getBytesCached()) +
                    "\nCached:" + cachedVideo.getPercentageCached() +
                    "\nClick to remove");

            LinearLayout ll = findViewById(R.id.linearlayout);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            ll.addView(myButton, lp);

            // Remove the asset when clicking the  button
            myButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    cachedVideo.remove();
                    TaskOverview.super.onBackPressed();
                    TaskOverview.this.finish();
                }
            });

        }

        // Back to MainMenu
        backbutton = findViewById(R.id.backbutton);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskOverview.super.onBackPressed();
                TaskOverview.this.finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
