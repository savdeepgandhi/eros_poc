package com.theoplayer.poc_offlineplayback;

import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.theoplayer.android.api.THEOplayerGlobal;
import com.theoplayer.android.api.THEOplayerView;
import com.theoplayer.android.api.cache.CachingParameters;
import com.theoplayer.android.api.cache.CachingTask;
import com.theoplayer.android.api.cache.CachingTaskStatus;
import com.theoplayer.android.api.event.EventListener;
import com.theoplayer.android.api.event.cache.task.CachingTaskEventTypes;
import com.theoplayer.android.api.event.cache.task.CachingTaskStateChangeEvent;
import com.theoplayer.android.api.player.track.texttrack.TextTrackKind;
import com.theoplayer.android.api.source.SourceDescription;
import com.theoplayer.android.api.source.TextTrackDescription;
import com.theoplayer.android.api.source.drm.DRMConfiguration;
import com.theoplayer.android.api.source.drm.KeySystemConfiguration;
import com.theoplayer.android.api.source.drm.LicenseType;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import static com.theoplayer.android.api.source.SourceDescription.Builder.sourceDescription;
import static com.theoplayer.android.api.source.TypedSource.Builder.typedSource;
import static com.theoplayer.android.api.source.drm.DRMConfiguration.Builder.widevineDrm;

/**
 * Demo App Created by Savdeep Singh Gandhi on 17th April 2019
 * Copyright © 2019 Theoplayer.com. All rights reserved.
 */

public class OfflinePlaybackWithSubtitles extends AppCompatActivity {

    THEOplayerView theoPlayerView;
    Button backbutton;
    THEOplayerGlobal theoPlayerGlobal;

    // Video source
    static String videoSource = "https://content.uplynk.com/ext/12ccd788e5224b0692264ac02cc4929f/stg-6845260.mpd?rmt=wv&rmp.policy_overrides.can_persist=True&rmp.policy_overrides.can_play=True&rmp.policy_overrides.rental_duration_seconds=86400&rmp.policy_overrides.license_duration_seconds=28800&rmp.policy_overrides.playback_duration_seconds=14400&rays=a";
    static String widevineLA = "https://content.uplynk.com/wv";
    static String videoName = "Shubh Mangal Saavdhan";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //   Initialize variables & player
          theoPlayerGlobal = THEOplayerGlobal.getSharedInstance(this);
          setContentView(R.layout.activity_offlinesubtitles);
          theoPlayerView = findViewById(R.id.theoplayer);
          theoPlayerView.getSettings().setFullScreenOrientationCoupled(true);
          playSourceFromCache(videoSource);


        // Back to the main menu
        backbutton = findViewById(R.id.backbutton);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OfflinePlaybackWithSubtitles.this.onBackPressed();
                OfflinePlaybackWithSubtitles.this.finish();
            }
        });


    }

    // Simple method to get the Downloaded subtitle file
    private String getLocalPathForSubtitles(String videoName, String language) {
        //String basedir = Environment.getExternalStorageDirectory().getAbsolutePath();
        String filename = (videoName.toLowerCase().replace(' ', '-')).concat("_").concat(language).concat(".webvtt");
        File file = new File(this.getExternalFilesDir(null) + "/" +Environment.DIRECTORY_DOWNLOADS + "/" + filename);
        return "file:///" + file.getAbsolutePath();
    }

    // Build DRM configuration for WideVine
    private DRMConfiguration createDrmConfiguration() {
        KeySystemConfiguration widevine = KeySystemConfiguration.Builder
                .keySystemConfiguration(widevineLA)
                .headers(Collections.singletonMap("X-DASH-SEND-ALL-KEYS","1"))
                .licenseType(LicenseType.PERSISTENT)
                .build();
        return widevineDrm(widevine).build();
    }

    // To playback cached material, create and play the stream as per usual. The cached content will be used for the known source.
    private void playSourceFromCache(String src) {
        DRMConfiguration drmConf = createDrmConfiguration();
        SourceDescription sourceDescription = sourceDescription(typedSource(src).drm(drmConf).build())
                .textTracks(new TextTrackDescription(
                                this.getLocalPathForSubtitles(videoName, "en"),
                                true,  TextTrackKind.SUBTITLES, "en","English"
                        )
                )
                .build();
        theoPlayerView.getPlayer().setSource(sourceDescription);
        theoPlayerView.getPlayer().play();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (theoPlayerView != null) {
            theoPlayerView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (theoPlayerView != null) {
            theoPlayerView.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (theoPlayerView != null) {
            theoPlayerView.onDestroy();
        }
    }
}

