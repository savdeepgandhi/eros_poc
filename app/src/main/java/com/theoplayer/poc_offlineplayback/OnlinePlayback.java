package com.theoplayer.poc_offlineplayback;

import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.theoplayer.android.api.THEOplayerGlobal;
import com.theoplayer.android.api.THEOplayerView;
import com.theoplayer.android.api.cache.CachingParameters;
import com.theoplayer.android.api.cache.CachingTask;
import com.theoplayer.android.api.cache.CachingTaskStatus;
import com.theoplayer.android.api.event.EventListener;
import com.theoplayer.android.api.event.cache.task.CachingTaskEventTypes;
import com.theoplayer.android.api.event.cache.task.CachingTaskProgressEvent;
import com.theoplayer.android.api.event.cache.task.CachingTaskStateChangeEvent;
import com.theoplayer.android.api.player.track.texttrack.TextTrackKind;
import com.theoplayer.android.api.source.SourceDescription;
import com.theoplayer.android.api.source.SourceType;
import com.theoplayer.android.api.source.TextTrackDescription;
import com.theoplayer.android.api.source.TypedSource;
import com.theoplayer.android.api.source.drm.DRMConfiguration;
import com.theoplayer.android.api.source.drm.KeySystemConfiguration;
import com.theoplayer.android.api.source.drm.LicenseType;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import static com.theoplayer.android.api.source.SourceDescription.Builder.sourceDescription;
import static com.theoplayer.android.api.source.TypedSource.Builder.typedSource;
import static com.theoplayer.android.api.source.drm.DRMConfiguration.Builder.playreadyDrm;
import static com.theoplayer.android.api.source.drm.DRMConfiguration.Builder.widevineDrm;

/**
 * Demo App Created by Savdeep Singh Gandhi on 17th April 2019
 * Copyright © 2019 Theoplayer.com. All rights reserved.
 */

public class OnlinePlayback extends AppCompatActivity {

    THEOplayerView theoPlayerView;
    Button backbutton;
    Button downloadVideo;
    Button Resumebutton;
    Button Pausebutton;
    THEOplayerGlobal theoPlayerGlobal;
    CachingTask cachingTask;

    // Video source
    static String videoSource = "https://content.uplynk.com/ext/12ccd788e5224b0692264ac02cc4929f/stg-6845260.mpd?rmt=wv&rmp.policy_overrides.can_persist=True&rmp.policy_overrides.can_play=True&rmp.policy_overrides.rental_duration_seconds=86400&rmp.policy_overrides.license_duration_seconds=28800&rmp.policy_overrides.playback_duration_seconds=14400&rays=a";
    static String widevineLA = "https://content.uplynk.com/wv";
    static String videoName = "Shubh Mangal Saavdhan";
    static String downloadPercentage = "100%";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final TextView status2 = findViewById(R.id.status2);



        // Prerequisites: Check if the app has the required permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1024);

        }

        // This is not required for production environments, but may help in debugging
        WebView.setWebContentsDebuggingEnabled(true);

        // Init Theoplayer view
        theoPlayerGlobal = THEOplayerGlobal.getSharedInstance(this);
        setContentView(R.layout.activity_online);
        theoPlayerView = findViewById(R.id.theoplayer);
        theoPlayerView.getSettings().setFullScreenOrientationCoupled(true);

        // Build the source description & set it to the player
        TypedSource typedSource = TypedSource.Builder
                .typedSource()
                .src("https://content.uplynk.com/ext/12ccd788e5224b0692264ac02cc4929f/stg-6845260.mpd")
                .drm(widevineDrm(
                        new KeySystemConfiguration("https://content.uplynk.com/wv")
                        ).build()
                )
                .type(SourceType.DASH)
                .build();

        SourceDescription sourceDescription = SourceDescription.Builder
                .sourceDescription(typedSource)
                .textTracks(new TextTrackDescription(
                                "https://cdn.theoplayer.com/demos/theoplayer-web/1060465_6846477.webvtt",
                                true,  TextTrackKind.SUBTITLES, "en","English"
                        )
                )
                .textTracks(new TextTrackDescription(
                        "https://cdn.theoplayer.com/demos/theoplayer-web/1060465_6846478_96.webvtt",
                                false,  TextTrackKind.SUBTITLES, "ar","Arabic"
                        )
                )
                .build();
        theoPlayerView.getPlayer().setSource(sourceDescription);

        //Download Button - Download Video
        downloadVideo = findViewById(R.id.downloadVideo);
        downloadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCachingSource();

            }
        });
        // Pause video Button
        Pausebutton = findViewById(R.id.Pausebutton);
        Pausebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cachingTask != null ){
                    cachingTask.pause();
                   // status2.setText("Status: Paused");
                }else {
                 // startCachingSource();
                    cachingTask.start();
                }
            }
        });

        // Resume video Button
        Resumebutton = findViewById(R.id.Resumebutton);
        Resumebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cachingTask != null ){
                    cachingTask.start();
                }else {
                   startCachingSource();
                }

            }
        });

        // Back to the main menu
        backbutton = findViewById(R.id.backbutton);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnlinePlayback.this.onBackPressed();
                OnlinePlayback.this.finish();
            }
        });
    }

// Caching/Downloading a Video
    private void startCachingSource() {

        //Getting the current Date + 2 weeks
        int noOfDays = 14; //i.e two weeks
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, noOfDays);
        Date date = calendar.getTime();

        // Checking if video is already cached
        // (Note: initializing the cache happens async! - we init in Main Activity)
        CachingTask task = null;
        Iterator cachedVideos = theoPlayerGlobal.getCache().getTasks().iterator();
        while(cachedVideos.hasNext()) {
            CachingTask cachedVideo = (CachingTask) cachedVideos.next();
            if(cachedVideo.getSource().getSources().get(0).getSrc().equals(videoSource)) {
                task = cachedVideo;
                break;
            }
        }

        if(task != null) {
            // If the video was already cached and the caching task was finished, play the video
           final TextView status2 = findViewById(R.id.status2);
            if(task.getStatus().equals(CachingTaskStatus.DONE)) {
                playSourceFromCache(videoSource);
                status2.setText("Status: Downloaded");
                return;
            } else if(task.getStatus().equals(CachingTaskStatus.ERROR)) {
                task.remove();
            } else {
                // If the task was not finished yet, continue downloading
                task.start();
                task.addEventListener(CachingTaskEventTypes.CACHING_TASK_PROGRESS, new EventListener<CachingTaskProgressEvent>() {
                    @Override
                    public void handleEvent(CachingTaskProgressEvent cachingTaskProgressEvent) {
                        status2.setText("Status: " + new DecimalFormat("#.##").format(cachingTask.getPercentageCached()*100.00)+"%");
                    }
                });

                return;
            }
        }

        // If the video was not yet cached, create a new caching task
        cacheSource(
                videoSource,
                downloadPercentage,
                date
        );

        // For this proof-of-concept, also download the subtitles
        downloadSubtitle("https://s3-eu-west-1.amazonaws.com/theoplayer-cdn/demos/theoplayer-web/1060465_6846477.webvtt", videoName, "en");
        downloadSubtitle("https://s3-eu-west-1.amazonaws.com/theoplayer-cdn/demos/theoplayer-web/1060465_6846478_96.webvtt", videoName, "ar");

    }

    private void cacheSource(String src, String amount, Date expirationDate) {
        DRMConfiguration drmConfig = createDrmConfiguration();
        SourceDescription source = sourceDescription(typedSource(src).drm(drmConfig).build()).build();
        //CachingParameters cachingParameters = new CachingParameters(amount, expirationDate, 0);

        CachingParameters cachingParameters = new CachingParameters.Builder()
                .amount(amount)
                .expirationDate(expirationDate)
                // Lowest quality
                //.bandwidth(Long.valueOf(0))
                // Highest quality (Default)
                //.bandwidth(Long.valueOf(null))
                // Custom quality
                // .bandwidth(Long.valueOf(<customValue>))
                // Sidenote: for Verizon, download qualities are set as a parameter in the download URL with query parameters
                .build();
        cachingTask = theoPlayerGlobal.getCache().createTask(source, cachingParameters);
        final TextView status2 = findViewById(R.id.status2);
        cachingTask.addEventListener(CachingTaskEventTypes.CACHING_TASK_PROGRESS, new EventListener<CachingTaskProgressEvent>() {

            @Override
            public void handleEvent(CachingTaskProgressEvent cachingTaskProgressEvent) {
                switch (cachingTask.getStatus()) {
                    case DONE:
                        // handle success..
                        status2.setText("Status: Downloaded");
                        playSourceFromCache(videoSource);
                        break;
                    case ERROR:
                        // handle error..
                        status2.setText("Status: Download Failed");
                        break;
                    default: // do nothing
                        status2.setText("Status: " + new DecimalFormat("#.##").format(cachingTask.getPercentageCached()*100.00)+"%");
                        break;
                }
            }
        });
        cachingTask.start();

    }

    /**
     *
     * @param subtitleUrl | URL to one specific webvtt subtitle file
     * @param videoName | Video name
     * @param languageCode | ISO 2 character language code - used for saving different languages per video as example
     */
    private void downloadSubtitle(String subtitleUrl, String videoName, String languageCode) {
        String filename = (videoName.toLowerCase().replace(' ', '-')).concat("_").concat((languageCode.toLowerCase())).concat(".webvtt");

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(subtitleUrl));
        request.setMimeType("text/vtt");
        request.setDescription("Downloading subtitles (".concat(languageCode).concat(")..."));
        request.setTitle(filename);

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, filename);


        DownloadManager dm = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
        dm.enqueue(request);
    }


    // Simple sample method to get the current subtitle file
    private String getLocalPathForSubtitles(String videoName, String language) {
        //String basedir = Environment.getExternalStorageDirectory().getAbsolutePath();
        String filename = (videoName.toLowerCase().replace(' ', '-')).concat("_").concat(language).concat(".webvtt");
        File file = new File(this.getExternalFilesDir(null) + "/" +Environment.DIRECTORY_DOWNLOADS + "/" + filename);
        return "file:///" + file.getAbsolutePath();
    }

    // Build DRM configuration for WideVine
    private DRMConfiguration createDrmConfiguration() {
        KeySystemConfiguration widevine = KeySystemConfiguration.Builder
                .keySystemConfiguration(widevineLA)
                .headers(Collections.singletonMap("X-DASH-SEND-ALL-KEYS","1"))
                .licenseType(LicenseType.PERSISTENT)
                .build();
        return widevineDrm(widevine).build();
    }

    // To playback cached material, create and play the stream as per usual. The cached content will be used for the known source.
    private void playSourceFromCache(String src) {
        DRMConfiguration drmConf = createDrmConfiguration();
        SourceDescription sourceDescription = sourceDescription(typedSource(src).drm(drmConf).build())
                .textTracks(new TextTrackDescription(
                                this.getLocalPathForSubtitles(videoName, "en"),
                                true,  TextTrackKind.SUBTITLES, "en","English"
                        )
                )
                .textTracks(new TextTrackDescription(
                                this.getLocalPathForSubtitles(videoName, "ar"),
                                false,  TextTrackKind.SUBTITLES, "ar","Arabic"
                        )
                )
                .build();
        theoPlayerView.getPlayer().setSource(sourceDescription);
        theoPlayerView.getPlayer().play();
    }

    // Handle permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1024:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Back to main menu
                    backbutton = findViewById(R.id.backbutton);
                    backbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            OnlinePlayback.super.onBackPressed();
                            OnlinePlayback.this.finish();
                        }
                    });
                } else {
                    OnlinePlayback.super.onBackPressed();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (theoPlayerView != null) {
            theoPlayerView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (theoPlayerView != null) {
            theoPlayerView.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        theoPlayerView.onDestroy();
    }
}
