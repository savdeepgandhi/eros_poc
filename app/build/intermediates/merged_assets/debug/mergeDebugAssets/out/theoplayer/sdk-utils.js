var TheoplayerSdkUtils = /** @class */ (function () {
    function TheoplayerSdkUtils() {
        var _this = this;
        this.theoplayerJsToJavaEventListeners = {};
        this.setTargetQuality = function (track, qualityIDs) {
            var ids = Array.isArray(qualityIDs) ? qualityIDs : [qualityIDs];
            track.targetQuality = ids.map(function (qualityID) {
                var qualityIndex = track.qualities.map(function (quality) { return quality.id; }).indexOf(qualityID);
                return track.qualities[qualityIndex];
            });
        };
        this.addSdkEventListener = function (type, dispatcher, listenerMapKey, listener, debug) {
            if (debug === void 0) { debug = false; }
            if (typeof dispatcher == 'undefined' || !dispatcher) {
                if (debug) {
                    window.console.error('addSdkEventListener failed because of invalid dispatcher, type [' + type + '], listenerMapKey [' + listenerMapKey + ']');
                }
                return;
            }
            _this.theoplayerJsToJavaEventListeners[listenerMapKey] = _this.theoplayerJsToJavaEventListeners[listenerMapKey] || {};
            _this.theoplayerJsToJavaEventListeners[listenerMapKey][type] = _this.theoplayerJsToJavaEventListeners[listenerMapKey][type] || {};
            _this.theoplayerJsToJavaEventListeners[listenerMapKey][type] = listener;
            dispatcher.addEventListener(type, _this.theoplayerJsToJavaEventListeners[listenerMapKey][type]);
        };
        this.removeSdkEventListener = function (type, dispatcher, listenerMapKey) {
            var listenersForDispatcher = _this.theoplayerJsToJavaEventListeners[listenerMapKey];
            var listenerForDispatcherForType = listenersForDispatcher && listenersForDispatcher[type];
            if (listenerForDispatcherForType) {
                if (dispatcher) {
                    dispatcher.removeEventListener(type, listenerForDispatcherForType);
                }
                delete _this.theoplayerJsToJavaEventListeners[listenerMapKey][type];
            }
        };
        this.removeAllSdkEventListeners = function (dispatcher, listenerMapKey) {
            if (dispatcher) {
                var theoplayerJsToJavaEventListeners = _this.theoplayerJsToJavaEventListeners;
                if (theoplayerJsToJavaEventListeners && theoplayerJsToJavaEventListeners[listenerMapKey]) {
                    for (var type in theoplayerJsToJavaEventListeners[listenerMapKey]) {
                        dispatcher.removeEventListener(type, theoplayerJsToJavaEventListeners[listenerMapKey][type]);
                    }
                    delete theoplayerJsToJavaEventListeners[listenerMapKey];
                }
            }
        };
    }
    TheoplayerSdkUtils.prototype.setSource = function (source) {
        theoplayerEventProcessors.onSourceSet();
        player.source = source;
    };
    return TheoplayerSdkUtils;
}());
var theoplayerSdkUtils = new TheoplayerSdkUtils();
