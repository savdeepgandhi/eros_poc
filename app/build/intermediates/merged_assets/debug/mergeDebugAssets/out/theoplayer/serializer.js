var TimeRangesReplacer = /** @class */ (function () {
    function TimeRangesReplacer() {
    }
    TimeRangesReplacer.prototype.isReplaceable = function (value) {
        return typeof value != "undefined" && value != null &&
            typeof value.start == "function" &&
            typeof value.end == "function" &&
            typeof value.length == "number";
    };
    TimeRangesReplacer.prototype.replace = function (timeRanges) {
        var result = [];
        for (var i = 0; i < timeRanges.length; i++) {
            result[i] = {
                start: timeRanges.start(i),
                end: timeRanges.end(i)
            };
        }
        return result;
    };
    return TimeRangesReplacer;
}());
var CompositeReplacer = /** @class */ (function () {
    function CompositeReplacer(replacers) {
        this.replacers = replacers;
    }
    CompositeReplacer.prototype.isReplaceable = function (value) {
        return this.replacers.some(function (replacer) { return replacer.isReplaceable(value); });
    };
    CompositeReplacer.prototype.replace = function (value) {
        if (!value) {
            return undefined;
        }
        for (var _i = 0, _a = this.replacers; _i < _a.length; _i++) {
            var replacer = _a[_i];
            if (replacer.isReplaceable(value)) {
                return replacer.replace(value);
            }
        }
        return undefined;
    };
    return CompositeReplacer;
}());
var AndroidSdkSerializer = /** @class */ (function () {
    function AndroidSdkSerializer() {
    }
    // based on https://github.com/isaacs/json-stringify-safe/blob/02cfafd45f06d076ac4bf0dd28be6738a07a72f9/stringify.js#L4
    // which is used in NodeJS and detects and replaces circular references
    AndroidSdkSerializer.jsonToString = function (obj, replacer) {
        if (replacer === void 0) { replacer = AndroidSdkSerializer.DEFAULT_REPLACER; }
        return JSON.stringify(obj, this.serializer(function (key, value) {
            if (replacer.isReplaceable(value)) {
                return replacer.replace(value);
            }
            else {
                return value;
            }
        }));
    };
    ;
    AndroidSdkSerializer.DEFAULT_REPLACER = new CompositeReplacer([
        new TimeRangesReplacer(),
    ]);
    AndroidSdkSerializer.serializer = function (replacer, cycleReplacer) {
        var stack = [], keys = [];
        if (cycleReplacer == null)
            cycleReplacer = function (key, value) {
                if (stack[0] === value)
                    return "[Circular ~]";
                return "[Circular ~." + keys.slice(0, stack.indexOf(value)).join(".") + "]";
            };
        return function (key, value) {
            if (stack.length > 0) {
                var thisPos = stack.indexOf(this);
                ~thisPos ? stack.splice(thisPos + 1) : stack.push(this); //if 'this' is not in stack, then clear stack, otherwise, add 'this' to stack
                ~thisPos ? keys.splice(thisPos, Infinity, key) : keys.push(key);
                if (~stack.indexOf(value))
                    value = cycleReplacer.call(this, key, value);
            }
            return replacer == null ? value : replacer.call(this, key, value);
        };
    };
    return AndroidSdkSerializer;
}());
